var nconf = require('nconf');
nconf.argv().env();
nconf.file({
    file: __dirname + '/config.json'
});

// choose tool from CLI arguments - user input
var tool = '';
process.argv.forEach(function (val, index, array) {
	if(val.indexOf('=') !== -1) {
		var split = val.split('=');
		if(split[0] === 'tool') {
			tool = split[1];
		}
	}
});
if(tool === '') {
	console.log('Choose tool by adding argument tool, eg.: node tools.js tool=montage. Available tools: printDate, montage.');
}

// require modules
var PrintDateOnImages = require(__dirname + '/modules/PrintDateOnImages.js');
var CreateMontage = require(__dirname + '/modules/CreateMontage.js');

// execute tool
var toolConstructor;
if(tool === 'printDate') {
	var printOnDateOptions = {
		pathsImages: nconf.get('paths').source,
		pathsTarget: nconf.get('paths').target,
		fontSize: nconf.get('font').size,
		fontFile: nconf.get('font').file,
		fontColour: nconf.get('font').colour,
		fontStroke: nconf.get('font').stroke,
		maxSize: nconf.get('sizes').maxSize,
		datePadding: nconf.get('sizes').printDatePadding
	};

	var toolConstructor = new PrintDateOnImages(printOnDateOptions);
} else if(tool === 'montage') {
	var montageOptions = {
		pathsImages: nconf.get('paths').source,
		pathsTarget: nconf.get('paths').target,
		maxSize: nconf.get('sizes').maxSize,
		datePadding: nconf.get('sizes').montagePadding
	};

	var toolConstructor = new CreateMontage(montageOptions);
}

if(typeof toolConstructor.process === 'function') {
	toolConstructor.process();
}