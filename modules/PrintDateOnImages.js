var fs = require('fs');
var gm = require('gm');
var async = require('async');

function PrintDateOnImages(options) {
	this.paths = {
		images: options.pathsImages || __dirname + '/images/',
		target: options.pathsTarget || __dirname + '/processed/',
		fonts: __dirname + '/fonts/'
	};
	this.font = {
		size: options.fontSize || 36,
		file: options.fontFile || 'Arial Black.ttf',
		colour: options.fontColour || '#FFFFFF',
		strokeColour: options.fontStroke || '#000000'
	};
	this.sizes = {
		maxSize: options.maxSize || 1800,
		padding: options.datePadding || 40
	};
	this.seriesLength = 0;
	this.seriesCurrent = 0;

	this.stringsSizesCache = {};
};

PrintDateOnImages.prototype.process = function() {
	var self = this;

	fs.readdir(this.paths.images, function(err, files) {
		var filesCallbacks = [];

		if(err) {console.log(err);}

		for(var i=0; i<files.length; i++) {
			if(files[i].indexOf('.jpg') !== -1) {
				var printDateReturn = self.printDateCallback(files[i]);
				filesCallbacks.push(printDateReturn);
			}
		}
		
		console.log('***', filesCallbacks.length + ' files to process');
		self.seriesLength = filesCallbacks.length;

		console.log('*** processing started');
		async.series(filesCallbacks, function() {
			console.log('*** processing finished');
		});
	});
};

PrintDateOnImages.prototype.determineTextWidth = function(string, callback) {
	var self = this;

	if(typeof this.stringsSizesCache[string] !== 'undefined') {
		if(typeof callback === 'function') {
			callback(this.stringsSizesCache[string]);
		} else {
			console.log('Text size: '+size.width+'x'+size.height);
		}
	} else {
		// create dummy image
		gm(1000,100,'#cccccc')
		.fill(self.font.colour).stroke(self.font.strokeColour)
		.font(self.paths.fonts + self.font.file, self.font.size)
		.drawText(0, self.font.size + 5, string)
		.trim()
		.toBuffer('PNG', function(err, buffer) {
			gm(buffer).size(function(err, size) {
				self.stringsSizesCache[string] = {};
				self.stringsSizesCache[string].width = size.width;
				self.stringsSizesCache[string].height = size.height;

				if(typeof callback === 'function') {
					callback(size);
				} else {
					console.log('Text size: '+size.width+'x'+size.height);
				}
			});
		});
	}


};

PrintDateOnImages.prototype.printDateCallback = function(file) {
	var self = this;

	return function(callback) {
		self.printDate(file, callback);
	}
};

PrintDateOnImages.prototype.printDate = function(file, callback) {
	var self = this;

	gm(this.paths.images + file).identify(function(err, value) {
		if (err) {
			console.dir(arguments);
			callback(null, '');
			return;
		};
		if (!value['Profile-EXIF']) {
			console.log('No EXIF available');
			callback(null, '');
			return;
		};
 		if (!value['Profile-EXIF']['Date Time Original']) {
 			console.log('No \'Date Time Original\' available in EXIF, can\'t print date');
			callback(null, '');
			return;
 		};

 		var context = this;

 		// prepare file options
 		var fileOpts = {
 			date: value['Profile-EXIF']['Date Time Original'].split(' ')[0].replace(/:/g, '-'),
 			fileWidth: value.size.width,
 			fileHeight: value.size.height
 		};
 		fileOpts.ratio = fileOpts.fileWidth / fileOpts.fileHeight > 1 ? (fileOpts.fileHeight / fileOpts.fileWidth).toFixed(3) : (fileOpts.fileWidth / fileOpts.fileHeight).toFixed(3)

 		// determine text width, print date on the image and save image
		self.determineTextWidth(fileOpts.date, function(size) {
			var textCoords = {};

			if(fileOpts.fileWidth / fileOpts.fileHeight > 1) {
				// landscape
				textCoords.x = self.sizes.maxSize - size.width - self.sizes.padding;
				textCoords.y = parseInt(self.sizes.maxSize*fileOpts.ratio) - self.sizes.padding;
			} else {
				// portrait
				textCoords.x = parseInt(self.sizes.maxSize*fileOpts.ratio) - size.width - self.sizes.padding;
				textCoords.y = self.sizes.maxSize - self.sizes.padding;
			}

			// save image
			context
			.filter('Triangle')
			.define('jpeg:fancy-upsampling=off')
			.interlace('None')
			.quality(90)
			.resize(self.sizes.maxSize,self.sizes.maxSize)
			.fill(self.font.colour).stroke(self.font.strokeColour)
			.font(self.paths.fonts + self.font.file, self.font.size)
			.drawText(textCoords.x, textCoords.y, fileOpts.date)
			.noProfile()
			.write(self.paths.target + file, function(err) {
				if (err) {
					console.dir(arguments)
				} else {
					self.seriesCurrent++;
					console.log('*** printed "'+fileOpts.date+'" and saved to '+file+' ('+self.seriesCurrent+'/'+self.seriesLength+')');
				}

				callback(null, '');
			});
		});
	});
};

module.exports = PrintDateOnImages;