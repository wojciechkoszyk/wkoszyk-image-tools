var fs = require('fs');
var gm = require('gm');
var async = require('async');

function CreateMontage(options) {
	this.paths = {
		images: options.pathsImages || __dirname + '/images/',
		target: options.pathsTarget || __dirname + '/processed/'
	};
	this.options = {
		maxSize: options.maxSize || 1800,
		padding: options.montagePadding || 0,
		tile: '2x2'
	};
	this.filesSorted = {};
	this.seriesCurrent = 0;
	this.seriesLength = 0;
};

CreateMontage.prototype.process = function() {
	var self = this;

	fs.readdir(this.paths.images, function(err, files) {
		var filesCallbacks = [];

		for(var i=0; i<files.length; i++) {
			if(files[i].indexOf('.jpg') !== -1) {
				var getRatioReturn = self.getRatioCallback(files[i]);
				filesCallbacks.push(getRatioReturn);
			}
		}
		
		console.log('***', filesCallbacks.length + ' files to process');
		self.seriesLength = filesCallbacks.length;

		console.log('*** starting reading images data from files');
		async.series(filesCallbacks, function() {
			console.log('*** reading images data finished');
			console.log('*** sorting files by ratio and orientation');
			var filesPrepared = [];

			for(var ratio in self.filesSorted) {
				for(var orientation in self.filesSorted[ratio]) {
					console.log('*** ratio', ratio, 'and', orientation, 'contains', self.filesSorted[ratio][orientation].length, 'files');
					var four = [];
					for(var i=0; i<self.filesSorted[ratio][orientation].length; i++) {
						four.push(self.filesSorted[ratio][orientation][i]);
						if((i+1)%4 === 0 || i === self.filesSorted[ratio][orientation].length-1) {
							filesPrepared.push(four);
							four = [];
						}
					}
				}
			}

			var montageCallbacks = [];
			for(var j=0; j<filesPrepared.length; j++) {
				var createMontageReturn = self.createMontageCallback(filesPrepared[j]);
				montageCallbacks.push(createMontageReturn);
			}
			
			self.seriesLength = filesPrepared.length;

			console.log('*** sorting finished, starting montage process');
			async.series(montageCallbacks, function() {
				console.log('*** processing finished');
			});
		});
	});
};

CreateMontage.prototype.getRatioCallback = function(file) {
	var self = this;

	return function(callback) {
		self.getRatio(file, callback);
	}
};

CreateMontage.prototype.getRatio = function(file, callback) {
	var self = this;

	gm(this.paths.images + file).identify(function(err, value) {
		if (err) {
			console.dir(arguments);
			callback(null, '');
			return;
		};

 		// prepare file options
 		var fileOpts = {
 			fileWidth: value.size.width,
 			fileHeight: value.size.height
 		};
 		fileOpts.file = file;
 		fileOpts.ratio = fileOpts.fileWidth / fileOpts.fileHeight > 1 ? (fileOpts.fileHeight / fileOpts.fileWidth).toFixed(2) : (fileOpts.fileWidth / fileOpts.fileHeight).toFixed(2)

 		if(fileOpts.fileWidth / fileOpts.fileHeight > 1) {
 			fileOpts.orientation = 'landscape';
 		} else {
 			fileOpts.orientation = 'portrait';
 		}

 		if(typeof self.filesSorted[fileOpts.ratio] === 'undefined') {
 			self.filesSorted[fileOpts.ratio] = {};
 		}
 		if(typeof self.filesSorted[fileOpts.ratio][fileOpts.orientation] === 'undefined') {
 			self.filesSorted[fileOpts.ratio][fileOpts.orientation] = [];
 		}
 		self.filesSorted[fileOpts.ratio][fileOpts.orientation].push(fileOpts);

 		console.log('*** read', fileOpts.file+', ratio:', fileOpts.ratio+', orientation:', fileOpts.orientation)

 		callback(null, '');
	});
};

CreateMontage.prototype.createMontageCallback = function(files) {
	var self = this;

	return function(callback) {
		self.createMontage(files, callback);
	}
};

CreateMontage.prototype.createMontage = function(files, callback) {
	var self = this;

	var filesLength = files.length;

	if(filesLength < 4) {
		var additional = 4-filesLength;
		for(var i=0; i<additional; i++) {
			files.push(files[0]);
		}
	}

	var number = 0;
	if(self.seriesCurrent < 10) {
		number = '000'+self.seriesCurrent;
	} else if (self.seriesCurrent > 9 && self.seriesCurrent < 100) {
		number = '00'+self.seriesCurrent;
	} else if (self.seriesCurrent > 99 && self.seriesCurrent < 1000) {
		number = '0'+self.seriesCurrent;
	} else {
		number = self.seriesCurrent;
	}

	var fileName = number+'.jpg';

	gm(this.options.maxSize, this.options.maxSize)
	.montage(self.paths.images + files[0].file)
	.montage(self.paths.images + files[1].file)
	.montage(self.paths.images + files[2].file)
	.montage(self.paths.images + files[3].file)
	.geometry('+'+this.options.padding+'+'+this.options.padding)
	.tile(this.options.tile)
	// .resize(this.options.maxSize, this.options.maxSize)
	// .crop(this.options.maxSize, this.options.maxSize*0.666, 0, 0)
	.write(this.paths.target + fileName, function(err) {
	    if (err) {
	    	console.dir(arguments)
	    } else {
	    	self.seriesCurrent++;
	    	console.log('*** created montage from', filesLength, 'files and saved to '+fileName+' ('+self.seriesCurrent+'/'+self.seriesLength+')');
	    }

		callback(null, '');
	});
};

module.exports = CreateMontage;